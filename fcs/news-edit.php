<?php require('header.php') ?>
<?php require('menu.php') ?>
<div class="content">
	<?php 
		require('sidebar.php'); 
		$id = $_GET['id'];
		$str = mysqli_query($connect, "SELECT * FROM news WHERE id_news = $id");
		$news = mysqli_fetch_array($str, MYSQLI_ASSOC);
	?>
	<div class="main"> 
		<form class="news_form" action="act_edit_news.php?id=<?php echo $news['id_news']; ?>" method="post">
			<label for="title"> Title </label>
			<input type="text" name="title" value="<?php echo $news['title'];?>">
			<label for="intro_text"> Intro text </label>
			<textarea name="intro_text"> <?php echo $news['intro_text']; ?> </textarea>
			<label for="full_text"> Full text </label>
			<textarea name="full_text"> <?php echo $news['full_text']; ?> </textarea>
			<input type="submit" name='submit' value="edit">
		</form>
	</div>
</div>
<?php require('footer.php'); ?>