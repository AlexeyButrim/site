<?php require('header.php') ?>
<?php require('menu.php') ?>
<?php require('slider.php') ?>
<div class="content">
	<?php require('sidebar.php') ?>
	<?php $id = $_GET['id']; ?>
	<div class="main">
		<?php require('categories.php') ?>
		<h2> Последние новости </h2>
		<h4> Категория: <?php echo $id; ?> <a class="add-news-link" href="./news-add.php?id=<?php echo $id; ?>"> (добавить новость) </a> </h4>
		<?php 
			require_once('connection.php');
			$last_news = mysqli_query($connect, "SELECT * FROM news WHERE category = '$id' ORDER BY id_news DESC LIMIT 5");
			$count = mysqli_num_rows($last_news);

			for ($i = 0; $i < $count; $i++) {
				mysqli_data_seek($last_news, $i);
				$row = mysqli_fetch_array($last_news, MYSQLI_ASSOC);
		?>

		<div class="material">
			<h3> <a href="news-view.php?id=<?php echo $row['id_news']; ?>"> <?php echo $row['title']?> </a> </h3>
			<p> <?php echo $row['intro_text']; ?> </p>
		</div>
		<?php } ?>
	</div>
</div>
<?php require('footer.php') ?>