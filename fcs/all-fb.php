<?php require('header.php') ?>
<?php require('menu.php') ?>
<div class="content">
	<?php require('sidebar.php'); ?>
	<div class="main"> 
		<?php require_once('connection.php'); ?>
		<h2> Обратная связь </h2>
		<?php $str = mysqli_query($connect, "SELECT * FROM feedback"); ?>
		<table class="db_table">
			<?php while($fb = mysqli_fetch_array($str)) { ?>
			<tr>
				<td> <?php echo $fb['id_fb']; ?> </td>
				<td> <?php echo $fb['subject']; ?> </td>
				<td> <?php echo $fb['email']; ?> </td>
				<td> <?php echo $fb['name']; ?> </td>
			</tr>
			<tr style="background: orange; color: black;">
				<td colspan="6" style="background: orange; color: black;"> 
				<p style="text-align: center;"> <?php echo $fb['full_text']; ?> </p> </td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
<?php require('footer.php'); ?>